/* Sources: 
https://www.rhyous.com/2011/11/13/how-to-read-a-pcap-file-from-wireshark-with-c 
www.winpcap.org */




#define _GLIBCXX_USE_C99_DYNAMIC 1
#include "pcap.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <time.h>
#include <vector>
#include <fstream>

/* 4 bytes IP address */
typedef struct ip_address{
    u_char byte1;
    u_char byte2;
    u_char byte3;
    u_char byte4;
}ip_address;
using namespace std;
string file = "C:\\Users\\Shaun\\Desktop\\CyberSecurity\\dataset2.pcap";
char errbuff[PCAP_ERRBUF_SIZE];

struct pcap_pkthdr *header;
	// Use pcap_open_offline
	// http://www.winpcap.org/docs/docs_41b5/html/group__wpcapfunc.html#g91078168a13de8848df2b7b83d1f5b69
	pcap_t * pcap = pcap_open_offline(file.c_str(), errbuff);
const u_char *data;

/* IPv4 header */
typedef struct ip_header{
    u_char  ver_ihl;        // Version (4 bits) + Internet header length (4 bits)
    u_char  tos;            // Type of service
    u_short tlen;           // Total length
    u_short identification; // Identification
    u_short flags_fo;       // Flags (3 bits) + Fragment offset (13 bits)
    u_char  ttl;            // Time to live
    u_char  proto;          // Protocol
    u_short crc;            // Header checksum
    ip_address  saddr;      // Source address
    ip_address  daddr;      // Destination address
    u_int   op_pad;         // Option + Padding
     u_short sport;          // Source port
    u_short dport;          // Destination port
}ip_header;



/*********************************************************************************/
//Functions you should use

int packet_flow_time(const u_char *pkt_data, int timestamp){

char timestr[16];
int difference = (header->ts.tv_sec )- timestamp;
//cout<< header->ts.tv_sec << " timestamp: " << timestamp<<endl;
if (difference > 60){

/*initialize timestamp = 0 outside the while loop. inside the loop,
set timestamp equal to this function.  If the difference is 60,
a minute passed by between the sets of packets. Then we update our list */

timestamp = header->ts.tv_sec;
cout<<"Current time in packets: "<< timestamp<<endl;
}



return timestamp;



}






/*********************************************************************************/

int main()
{

struct bpf_program fcode;


u_int packetCount = 0;
int timestamp = 0;
vector <int> sipvec;
vector<int> dipvec;
vector<int> sipcount;
vector<int> risksip;
vector<int> riskdip;


/* This code is used by Shaun Miller in his Cryber Security Project*/
pcap_compile(pcap, &fcode, "udp", 1, 0);
pcap_setfilter(pcap, &fcode);
ofstream myfile;
myfile.open("UDP_results.txt");
myfile << "UDP packets were considered on intervals of 60 seconds."<<endl;
myfile<< "If source ip was present more than 10000 time during this interval it was recorded."<<endl<<endl;

while (pcap_next_ex(pcap, &header, &data) >= 0)
{
int j = 0;
int holdtime;
ip_header *ih;
ih = (ip_header *) (data +
        14);


        int s1 = ih ->saddr.byte1;
        int s2 = ih ->saddr.byte2;
        int s3 = ih ->saddr.byte3;
        int s4 = ih ->saddr.byte4;
        int time = header->ts.tv_sec;
    for(int k = 0; k < sipcount.size(); k++){
        if(s1 == sipvec[5*k] && s2 == sipvec[5*k +1] && s3 ==sipvec[5*k+2] && s4 == sipvec[5*k +3]){
            sipcount[k]++;
            j = 1;
        }

    }

    if(j == 0){

        const int holds1 = s1;
        const int holds2 = s2;
        const int holds3 = s3;
        const int holds4 = s4;



        sipvec.push_back(holds1);
         sipvec.push_back(holds2);
         sipvec.push_back(holds3);
         sipvec.push_back(holds4);
         sipvec.push_back(time);





        sipcount.push_back(1);
    }
         holdtime = packet_flow_time(data,timestamp);

         if(holdtime != timestamp){
            timestamp = holdtime;
            for (int k = 0; k< sipcount.size(); k++){
                if(sipcount[k] > 10000){

                    risksip.push_back(sipvec[5*k]);
                    risksip.push_back(sipvec[5*k+1]);
                    risksip.push_back(sipvec[5*k+2]);
                    risksip.push_back(sipvec[5*k+3]);
                    risksip.push_back(sipvec[5*k+4]);




                }

            }
            cout<< "Size of vector during interval: "<<sipvec.size()<<endl;
                sipvec.clear();
                sipcount.clear();

                cout<< "Ip addresses labeled at risk (with repeats): "<< risksip.size()/5<<endl;
                cout<< endl;

         }


packetCount++;
        /*from here I will consider the vector of the source
        ips that denote possible victims. Each entry had a at least
        ten thousand packets sent over the course of a minute.*/

}

myfile <<"Total Number of UDP Packets: "<< packetCount<<endl<<endl;


cout<< "done"<< packetCount<< endl;
vector <int> victcount;
vector<int> victvec;
for(int i = 0; i< risksip.size()/5; i++){
    victcount.push_back(1);
    for(int j = 0; j <risksip.size() && j != i; j++){
        if(risksip[5*i]==risksip[5*j] &&
           risksip[5*i+1]==risksip[5*j+1] &&
           risksip[5*i+2] == risksip[5*j+2] &&
        risksip[5*i+3]==risksip[5*j+3] ) {

        victcount[i]++;

        }

    }




myfile<< risksip[5*i]<<"." <<risksip[5*i+1]<<"."<<risksip[5*i+2]<<"."<<risksip[5*i+3] << " --> ";
    /*myfile<<riskdip[4*i]<<"."<<riskdip [4*i +1] << "."<<riskdip[4*i+2]<< "."<<riskdip[4*i+3]<< "\\";*/
    myfile<<" Time Recorded: "<< risksip[5*i+4] << " Repeated: "<<victcount[i]<<endl;

}


myfile.close();

myfile.open("Source_ips_UDP.txt");
for(int i = 0; i<risksip.size()/5; i++){
   myfile<< risksip[5*i]<<"." <<risksip[5*i+1]<<"."<<risksip[5*i+2]<<"."<<risksip[5*i+3] <<endl;

}
myfile.close();

}


/*********************************************************************************/










